import 'package:flutter/material.dart';
import 'package:web_demo/Util/constant.dart';
class GlobalWidget{
  static customAppbar(String title) => AppBar(
        title:  Text(title,style: TextStyle(color: Colors.white,fontSize: 16)),
        backgroundColor: bgColor,
        centerTitle: true,
        elevation: 0,
      ); 

      static textFormField(
      {int? line,
      bool obsCureText = false,
      FocusNode? focusNode,
      TextInputType? textInputType = TextInputType.text,
      TextEditingController? controller,
      String? labelText,
      String? Function(String?)? validator,
      Widget? suffixIcon,
      Widget? prefixIcon,
      int? maxlength,
      AutovalidateMode? autovalidateMode,
      bool readOnly = false}) {
    return TextFormField(
      focusNode: focusNode,
      maxLines: line,
      autovalidateMode: autovalidateMode,
      obscureText: obsCureText,
      obscuringCharacter: '*',
      controller: controller,
      cursorColor: Colors.black,
      keyboardType: textInputType,
      maxLength: maxlength,
      decoration: InputDecoration(
        prefixIcon: prefixIcon,
        hintText: labelText,
        filled: true,
        counterText: "",
        fillColor: Color(0xfff2f2f2),
        suffixIcon: suffixIcon,
        hintStyle: TextStyle(
            color: Color(0xff808080),
            fontWeight: FontWeight.w400,
            fontFamily: "CircularStd",
            fontStyle: FontStyle.normal,
            fontSize: 16),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(style: BorderStyle.none),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(15),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(15),
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide.none),
      ),
      validator: validator,
      readOnly: readOnly,
    );
  }
}