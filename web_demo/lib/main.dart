import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'package:web_demo/MVC/RestaurentModule/View/restaurent_page.dart';
import 'package:web_demo/Util/constant.dart';


main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  runApp(MyApp());
}
initServices() async {
 
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(5120, 2880), // Zeplin UI size
      builder: () => GetMaterialApp(
      debugShowCheckedModeBanner: false,
       home: MyHomePage(),
      title: 'Web Admin Dashboard',
      theme: ThemeData(
        primaryColor: secondaryColor,
        backgroundColor: bgColor
        ),
      ),
    );
  }
}
