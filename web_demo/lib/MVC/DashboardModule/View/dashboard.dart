import 'package:flutter/material.dart';
import 'package:web_demo/GlobalWidget/global_widget.dart';
import 'package:web_demo/Helper/responsive.dart';
import 'package:web_demo/MVC/DashboardModule/View/components/side_menu.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(Responsive.isMobile(context))
      return  Scaffold(
      appBar: GlobalWidget.customAppbar("Super Admin"),
      drawer:SideMenu(),
      body: Column(
        mainAxisAlignment:MainAxisAlignment.center,
        children:   [
            Center(child: const Text("Mobile Dash"))
        ],
      ),
    );
  else
   return Scaffold(
      appBar: GlobalWidget.customAppbar("Super Admin"),
      body: Row(
      children: [
        Expanded(
          child: SideMenu(),
        ),
        Expanded(
          flex: 5,
          child: Center(child: Text("Web Dash Board"),),)
      ]
      ),
      
   );
  }
}