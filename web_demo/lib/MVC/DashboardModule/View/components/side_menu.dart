import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:web_demo/MVC/RestaurentModule/View/restaurent_page.dart';
import 'package:web_demo/Util/constant.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: bgColor,
        child: ListView(
          children: [
            DrawerHeader(
              child: Image.asset("assets/images/logo.png"),
            ),
            DrawerListTile(
              title: "Dashboard",
              svgSrc: "assets/icons/menu_dashbord.svg",
              press: () {
                
              },
              listChildren: [
                Text("1",style: TextStyle(color: Colors.white54,fontSize: 14.sp),
        ),
                Text("2",style: TextStyle(color: Colors.white54,fontSize: 14.sp),
        )
 
              ],

            ),
            DrawerListTile(
              title: "Restaurent",
              svgSrc: "assets/icons/menu_tran.svg",
              press: () {
                // Get.to(RestaurentPage());
              },
            ),
            DrawerListTile(
              title: "Order",
              svgSrc: "assets/icons/menu_task.svg",
              press: () {},
            ),
           
          ],
        ),
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
 const  DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.svgSrc,
    required this.press,
    this.listChildren
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback press;
  final List<Widget>? listChildren;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.all(8.h),
      child: ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        color: Colors.white54,
        height: 16,
      ),
      title: Text(
        title,
        style: TextStyle(color: Colors.white54),
      ),
    )
//       ExpansionTile(
//     title: Text(
//           title,
//           style: TextStyle(color: Colors.white54,fontSize: 14),
//         ),
      
//      backgroundColor: bgColor,
//      leading:  SvgPicture.asset(
//           svgSrc,
//           color: Colors.white54,
//           height: 16.h,
//         ),
//         collapsedIconColor: Colors.white54,
//   // children: listChildren??[]
// ),
    );
  }
}