import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_demo/GlobalWidget/global_widget.dart';
import 'package:web_demo/MVC/DashboardModule/View/dashboard.dart';
import 'package:web_demo/MVC/LoginModule/Controller/login_controller.dart';
import 'package:web_demo/Util/constant.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);

  final LoginController _lc = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("SSL Restaurant Admin Panel",),backgroundColor: bgColor,leading: Padding(
        padding:  EdgeInsets.only(left: 100.w),
        child: Image.asset("assets/images/logo.png",),
      ),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: Get.width / 2,
              child: GlobalWidget.textFormField(
                  controller: _lc.name, labelText: "Enter Your UserName"),
            ),
            Container(
              height: 100,
              width: Get.width / 2,
              child: GlobalWidget.textFormField(
                  controller: _lc.password, labelText: "Enter Your Passowrd"),
            ),
            InkWell(
              onTap: (){
                Get.to(Dashboard());
              },
              child: Container(
                height: 60,
                width: Get.width * 0.2,
                child: Center(
                  child: Text("Submit",
                      style: TextStyle(
                          color: const Color(0xffffffff),
                          fontWeight: FontWeight.w700,
                          fontFamily: "CircularStd",
                          fontStyle: FontStyle.normal,
                          fontSize: 16),
                      textAlign: TextAlign.center),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.r),
                  ),
                  color: bgColor,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
