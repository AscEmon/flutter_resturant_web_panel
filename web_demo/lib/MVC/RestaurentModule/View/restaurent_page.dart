// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:web_demo/GlobalWidget/global_widget.dart';
// import 'package:web_demo/Util/constant.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';


// class RestaurentPage extends StatelessWidget {
//    RestaurentPage({Key? key}) : super(key: key);
// HDTRefreshController _hdtRefreshController = HDTRefreshController();
//   static const int sortName = 0;
//   static const int sortStatus = 1;
//   bool isAscending = true;
//   int sortType = sortName;


//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: GlobalWidget.customAppbar("Restaurent panel"),
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           SizedBox(
//             height: 20,
//           ),
//           Padding(
//             padding:  EdgeInsets.only(left: 20.0),
//             child: Text("Restaurent panel",style: TextStyle(color: bgColor,fontSize: 16),),
//           ),
//          SizedBox(
//             height: 20,
//           ),
//           Container(
//             height: Get.height,

//             child: _getBodyWidget(context))
//         // Padding(
//         //   padding:  EdgeInsets.symmetric(horizontal: 20),
//         //   child: ClipRRect(
//         //     borderRadius: BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20)),
//         //     child: Container(
//         //       height: 50,
//         //       color: Colors.cyan,
//         //       child: Row(
//         //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         //         children: [
//         //           Container(
//         //            // height: 200,
//         //             width: MediaQuery.of(context).size.width * .22,
//         //             child: Center(
//         //               child: Text("Name"),
//         //             ),
//         //           ),
//         //           Container(
//         //           width: 1,
//         //           height: 50,
//         //           color: Color(0xffe2e2e2),
//         //         ),
//         //             Container(
//         //               //height: 200,
//         //                width: MediaQuery.of(context).size.width * .22,
//         //               child: Center(
//         //                 child: Text("Manager"),
//         //               ),
//         //             ),
//         //                Container(
//         //           width: 1,
//         //           height: 50,
//         //           color: Color(0xffe2e2e2),
//         //         ),
//         //             Container(
//         //              // height: 200,
//         //               width: MediaQuery.of(context).size.width * .22,
//         //               child: Center(
//         //                 child: Text("Number"),
//         //               ),
//         //             ),
//         //               Container(
//         //           width: 1,
//         //           height: 50,
//         //           color: Color(0xffe2e2e2),
//         //         ),
//         //             Container(
//         //              // height: 200,
//         //                width: MediaQuery.of(context).size.width * .22,
//         //               child: Center(
//         //                 child: Text("Action"),
//         //               ),
//         //             ),
//         //         ],
//         //       ),
//         //     ),
//         //   ),
//         // )
       
//       ],),
//     );
//   }
//    Widget _getBodyWidget(BuildContext context) {
//     return Container(
//       child: HorizontalDataTable(
//         leftHandSideColumnWidth: 100,
//         rightHandSideColumnWidth: 600,
//         isFixedHeader: true,
//         headerWidgets: _getTitleWidget(context),
//         leftSideItemBuilder: _generateFirstColumnRow,
//         rightSideItemBuilder: _generateRightHandSideColumnRow,
//         itemCount: user.userInfo.length,
//         rowSeparatorWidget: const Divider(
//           color: Colors.black54,
//           height: 1.0,
//           thickness: 0.0,
//         ),
//         leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
//         rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
//         verticalScrollbarStyle: const ScrollbarStyle(
//           thumbColor: Colors.yellow,
//           isAlwaysShown: true,
//           thickness: 4.0,
//           radius: Radius.circular(5.0),
//         ),
//         horizontalScrollbarStyle: const ScrollbarStyle(
//           thumbColor: Colors.red,
//           isAlwaysShown: true,
//           thickness: 4.0,
//           radius: Radius.circular(5.0),
//         ),
//         enablePullToRefresh: true,
//         refreshIndicator: const WaterDropHeader(),
//         refreshIndicatorHeight: 60,
//         onRefresh: () async {
//           //Do sth
//           await Future.delayed(const Duration(milliseconds: 500));
//           _hdtRefreshController.refreshCompleted();
//         },
//         htdRefreshController: _hdtRefreshController,
//       ),
//       height: MediaQuery.of(context).size.height,
//     );
//   }

//   List<Widget> _getTitleWidget(BuildContext context) {
//     return [
//       TextButton(
//         style: TextButton.styleFrom(
//           padding: EdgeInsets.zero,
//         ),
//         child: _getTitleItemWidget(
//             'Name' + (sortType == sortName ? (isAscending ? '↓' : '↑') : ''),
//             100),
//         onPressed: () {
//           sortType = sortName;
//           isAscending = !isAscending;
//           user.sortName(isAscending);
//           // setState(() {});
//         },
//       ),
//       TextButton(
//         style: TextButton.styleFrom(
//           padding: EdgeInsets.zero,
//         ),
//         child: _getTitleItemWidget(
//             'Status' +
//                 (sortType == sortStatus ? (isAscending ? '↓' : '↑') : ''),
//             100),
//         onPressed: () {
//           sortType = sortStatus;
//           isAscending = !isAscending;
//           user.sortStatus(isAscending);
//           // setState(() {});
//         },
//       ),
//       _getTitleItemWidget('Phone', 200),
//       _getTitleItemWidget('Register', 100),
//       _getTitleItemWidget('Termination', 200),
//     ];
//   }

//   Widget _getTitleItemWidget(String label, double width) {
//     return Container(
//       child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
//       width: width,
//       height: 56,
//       padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//       alignment: Alignment.centerLeft,
//     );
//   }

//   Widget _generateFirstColumnRow(BuildContext context, int index) {
//     return Container(
//       child: Text(user.userInfo[index].name),
//       width: 100,
//       height: 52,
//       padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//       alignment: Alignment.centerLeft,
//     );
//   }

//   Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
//     return Row(
//       children: <Widget>[
//         Container(
//           child: Row(
//             children: <Widget>[
//               Icon(
//                   user.userInfo[index].status
//                       ? Icons.notifications_off
//                       : Icons.notifications_active,
//                   color:
//                       user.userInfo[index].status ? Colors.red : Colors.green),
//               Text(user.userInfo[index].status ? 'Disabled' : 'Active')
//             ],
//           ),
//           width: 100,
//           height: 52,
//           padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//           alignment: Alignment.centerLeft,
//         ),
//         Container(
//           child: Text(user.userInfo[index].phone),
//           width: 200,
//           height: 52,
//           padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//           alignment: Alignment.centerLeft,
//         ),
//         Container(
//           child: Text(user.userInfo[index].registerDate),
//           width: 100,
//           height: 52,
//           padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//           alignment: Alignment.centerLeft,
//         ),
//         Container(
//           child: Text(user.userInfo[index].terminationDate),
//           width: 200,
//           height: 52,
//           padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//           alignment: Alignment.centerLeft,
//         ),
//       ],
//     );
//   }
// }




// User user = User();

// class User {
//   List<UserInfo> userInfo = [];

//   void initData(int size) {
//     for (int i = 0; i < size; i++) {
//       userInfo.add(UserInfo(
//           "User_$i", i % 3 == 0, '+001 9999 9999', '2019-01-01', 'N/A'));
//     }
//   }

//   ///
//   /// Single sort, sort Name's id
//   void sortName(bool isAscending) {
//     userInfo.sort((a, b) {
//       int aId = int.tryParse(a.name.replaceFirst('User_', '')) ?? 0;
//       int bId = int.tryParse(b.name.replaceFirst('User_', '')) ?? 0;
//       return (aId - bId) * (isAscending ? 1 : -1);
//     });
//   }

//   ///
//   /// sort with Status and Name as the 2nd Sort
//   void sortStatus(bool isAscending) {
//     userInfo.sort((a, b) {
//       if (a.status == b.status) {
//         int aId = int.tryParse(a.name.replaceFirst('User_', '')) ?? 0;
//         int bId = int.tryParse(b.name.replaceFirst('User_', '')) ?? 0;
//         return (aId - bId);
//       } else if (a.status) {
//         return isAscending ? 1 : -1;
//       } else {
//         return isAscending ? -1 : 1;
//       }
//     });
//   }
// }

// class UserInfo {
//   String name;
//   bool status;
//   String phone;
//   String registerDate;
//   String terminationDate;

//   UserInfo(this.name, this.status, this.phone, this.registerDate,
//       this.terminationDate);
// }

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:horizontal_data_table/refresh/hdt_refresh_controller.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, }) : super(key: key);
 

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  HDTRefreshController _hdtRefreshController = HDTRefreshController();

  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;

  @override
  void initState() {
    user.initData(100);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: _getBodyWidget(),
    );
  }

  Widget _getBodyWidget() {
    return Container(
      width: Get.width,
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 100,
        rightHandSideColumnWidth: 600,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: user.userInfo.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        verticalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.yellow,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.red,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: true,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
        onRefresh: () async {
          //Do sth
          await Future.delayed(const Duration(milliseconds: 500));
          _hdtRefreshController.refreshCompleted();
        },
        htdRefreshController: _hdtRefreshController,
      ),
      height: MediaQuery.of(context).size.height,
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: _getTitleItemWidget(
            'Name' + (sortType == sortName ? (isAscending ? '↓' : '↑') : ''),
            100),
        onPressed: () {
          sortType = sortName;
          isAscending = !isAscending;
          user.sortName(isAscending);
          setState(() {});
        },
      ),
      TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: _getTitleItemWidget(
            'Status' +
                (sortType == sortStatus ? (isAscending ? '↓' : '↑') : ''),
            100),
        onPressed: () {
          sortType = sortStatus;
          isAscending = !isAscending;
          user.sortStatus(isAscending);
          setState(() {});
        },
      ),
      _getTitleItemWidget('Phone', 200),
      _getTitleItemWidget('Register', 100),
      _getTitleItemWidget('Termination', 200),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
      width: width,
      height: 56,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(user.userInfo[index].name),
      width: 100,
      height: 52,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Icon(
                  user.userInfo[index].status
                      ? Icons.notifications_off
                      : Icons.notifications_active,
                  color:
                      user.userInfo[index].status ? Colors.red : Colors.green),
              Text(user.userInfo[index].status ? 'Disabled' : 'Active')
            ],
          ),
          width: 100,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(user.userInfo[index].phone),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(user.userInfo[index].registerDate),
          width: 100,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(user.userInfo[index].terminationDate),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
      ],
    );
  }
}

User user = User();

class User {
  List<UserInfo> userInfo = [];

  void initData(int size) {
    for (int i = 0; i < size; i++) {
      userInfo.add(UserInfo(
          "User_$i", i % 3 == 0, '+001 9999 9999', '2019-01-01', 'N/A'));
    }
  }

  ///
  /// Single sort, sort Name's id
  void sortName(bool isAscending) {
    userInfo.sort((a, b) {
      int aId = int.tryParse(a.name.replaceFirst('User_', '')) ?? 0;
      int bId = int.tryParse(b.name.replaceFirst('User_', '')) ?? 0;
      return (aId - bId) * (isAscending ? 1 : -1);
    });
  }

  ///
  /// sort with Status and Name as the 2nd Sort
  void sortStatus(bool isAscending) {
    userInfo.sort((a, b) {
      if (a.status == b.status) {
        int aId = int.tryParse(a.name.replaceFirst('User_', '')) ?? 0;
        int bId = int.tryParse(b.name.replaceFirst('User_', '')) ?? 0;
        return (aId - bId);
      } else if (a.status) {
        return isAscending ? 1 : -1;
      } else {
        return isAscending ? -1 : 1;
      }
    });
  }
}

class UserInfo {
  String name;
  bool status;
  String phone;
  String registerDate;
  String terminationDate;

  UserInfo(this.name, this.status, this.phone, this.registerDate,
      this.terminationDate);
}